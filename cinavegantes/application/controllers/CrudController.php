<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CrudController extends CI_Controller {
    public function __construct() {
        parent:: __construct();
        $this->load->model('Crud_model');
    }

	public function index($page='listinfo')
	{
        $data['title']= ucfirst($page);

        $data['result'] = $this->Crud_model->getAllData();

        $this->load->view('templates/header',$data);
        $this->load->view('layouts/'.$page, $data);
        $this->load->view('templates/footer');
    }
    
    public function newinfo($page='newinfo'){

        $data['title']= ucfirst($page);

        $this->load->view('templates/header',$data);
        $this->load->view('layouts/'.$page);
        $this->load->view('templates/footer');
    }

    public function create() {
        $this->Crud_model->createData();
        redirect("CrudController");
    }
    public function edit($id,$page='editinfo') {

        $data['row'] = $this->Crud_model->getData($id);

        $this->load->view('templates/header',$data);
        $this->load->view('layouts/'.$page,$data);
        $this->load->view('templates/footer');
    }
    public function update($id) {
        $this->Crud_model->updateData($id);
        redirect("CrudController");
    }
    public function delete($id) {
        $this->Crud_model->deleteData($id);
        redirect("CrudController");
    }
}
