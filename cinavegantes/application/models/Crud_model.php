<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Crud_model extends CI_Model {
    public function __construct() {
        $this->load->database();
    }
    function createData() {
        $data = array (
            'name' => $this->input->post('name'),
            'middlename' => $this->input->post('middlename'),
            'lastname' => $this->input->post('lastname'),
            'birthdate' => $this->input->post('birthdate'),
            'gender' => $this->input->post('gender'),
            'address' => $this->input->post('address'),
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
            'zipcode' => $this->input->post('state'),
            'movilenumber' => $this->input->post('movilenumber'),
            'phonenumber' => $this->input->post('phonenumber'),
            'worknumber' => $this->input->post('worknumber'),
            'company' => $this->input->post('company'),
            'curses' => $this->input->post('curses'),
            'comments' => $this->input->post('comments')
        );
        $this->db->insert('tbl_name', $data);
    }
    function getAllData() {
        $query = $this->db->query('SELECT * FROM users');
        return $query->result();
    }
    function getData($id) {
        $query = $this->db->query('SELECT * FROM users WHERE `id` =' .$id);
        return $query->row();
    }
    function updateData($id) {
        $data = array (
            'name' => $this->input->post('name'),
            'middlename' => $this->input->post('middlename'),
            'lastname' => $this->input->post('lastname'),
            'birthdate' => $this->input->post('birthdate'),
            'gender' => $this->input->post('gender'),
            'address' => $this->input->post('address'),
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
            'zipcode' => $this->input->post('state'),
            'movilenumber' => $this->input->post('movilenumber'),
            'phonenumber' => $this->input->post('phonenumber'),
            'worknumber' => $this->input->post('worknumber'),
            'company' => $this->input->post('company'),
            'curses' => $this->input->post('curses'),
            'comments' => $this->input->post('comments'),
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }
    function deleteData($id) {
        $this->db->where('id', $id);
        $this->db->delete('users');
    }
}